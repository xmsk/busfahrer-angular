import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MauMauComponent } from './mau-mau/mau-mau.component';



@NgModule({
  declarations: [
    MauMauComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MauMauComponent,
  ]
})
export class MauMauModule { }
