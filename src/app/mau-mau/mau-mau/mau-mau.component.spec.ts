import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MauMauComponent } from './mau-mau.component';

describe('MauMauComponent', () => {
  let component: MauMauComponent;
  let fixture: ComponentFixture<MauMauComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MauMauComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MauMauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
