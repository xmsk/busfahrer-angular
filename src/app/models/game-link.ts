// representation of a game
export interface GameLink {
  // name of the game
  name: string;
  // target link to refere to
  target: string;
}