import { Component } from '@angular/core';
import { GameLink } from './models/game-link';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'busfahrer-angular';
  // available games in the collection
  games: GameLink[] = [
    { name: 'Busfahrer', target: '/bus' },
    { name: 'Mau Mau', target: '/mau-mau' },
  ]
}
