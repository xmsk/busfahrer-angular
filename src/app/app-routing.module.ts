import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MauMauComponent } from './mau-mau/mau-mau/mau-mau.component';

const routes: Routes = [
  { path: '', redirectTo: 'bus', pathMatch: 'full' },
  {
    path: 'bus',
    loadChildren: () => import('./bus/bus.module').then(m => m.BusModule),
  },
  { path: 'mau-mau', component: MauMauComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
