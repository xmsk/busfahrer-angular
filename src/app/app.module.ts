import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameTileComponent } from './game-tile/game-tile.component';
import { BusModule } from './bus/bus.module';
import { MauMauModule } from './mau-mau/mau-mau.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    GameTileComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BusModule,
    MauMauModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
