import { Component, Input, OnInit } from '@angular/core';
import { GameLink } from '../models/game-link';

@Component({
  selector: 'app-game-tile',
  templateUrl: './game-tile.component.html',
  styleUrls: ['./game-tile.component.scss']
})
export class GameTileComponent implements OnInit {

  @Input() game?: GameLink;

  constructor() { }

  ngOnInit(): void {
  }

}
