import { Component, Input, OnInit } from '@angular/core';
import { Card } from '../models/card';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() card?: Card;

  constructor() { }

  ngOnInit(): void {
  }

  // Calculate the name of the image asset for the represented Card
  assetName() {
    if (this.card === undefined) {
      return 'assets/images/backside-card.png';
    }
    return `assets/images/${this.card.face.toLowerCase()}-${this.card.value.toLowerCase()}.png`;
  }

}
