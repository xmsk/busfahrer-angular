import { Component, Input, OnInit } from '@angular/core';
import { CardService } from '../services/card.service';

@Component({
  selector: 'app-game-controls',
  templateUrl: './game-controls.component.html',
  styleUrls: ['./game-controls.component.scss']
})
export class GameControlsComponent implements OnInit {

  @Input() cardsDealt: boolean = false;

  constructor(private cardService: CardService) { }

  ngOnInit(): void {
  }

  // confirm that all players have joined and start dealing the cards
  dealCards(): void {
    // todo: implement logic to deal cards
    this.cardsDealt = true;
    this.cardService.dealCards();
  }

  // temporary method to display the game controls
  // todo: remove
  undealCards(): void {
    this.cardsDealt = false;
  }

}
