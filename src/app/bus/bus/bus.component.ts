import { Component, OnInit } from '@angular/core';
import { Card } from '../models/card';
import { Face } from '../models/face';
import { Value } from '../models/value';

@Component({
  selector: 'app-bus',
  templateUrl: './bus.component.html',
  styleUrls: ['./bus.component.scss']
})
export class BusComponent implements OnInit {

  // cards to be displayed in the pyramide
  cards: Card[] = [
    { face: Face.Diamonds, value: Value.Seven },
    { face: Face.Diamonds, value: Value.Eight },
    { face: Face.Diamonds, value: Value.Nine },
    { face: Face.Diamonds, value: Value.Jack },
    { face: Face.Diamonds, value: Value.Queen },
    { face: Face.Diamonds, value: Value.King },
    { face: Face.Diamonds, value: Value.Ace },
    { face: Face.Hearts, value: Value.Seven },
    { face: Face.Hearts, value: Value.Eight },
    { face: Face.Hearts, value: Value.Nine },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
