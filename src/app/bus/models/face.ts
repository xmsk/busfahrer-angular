export enum Face {
  Clubs = "Clubs",
  Spades = "Spades",
  Hearts = "Hearts",
  Diamonds = "Diamonds",
}