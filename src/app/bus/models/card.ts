import { Face } from "./face";
import { Value } from "./value";

export interface Card {
  face: Face;
  value: Value;
}