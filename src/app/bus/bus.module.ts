import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

import { BusRoutingModule } from './bus-routing.module';
import { BusComponent } from './bus/bus.component';
import { CardComponent } from './card/card.component';
import { PyramideComponent } from './pyramide/pyramide.component';
import { GameControlsComponent } from './game-controls/game-controls.component';
import { CardService } from './services/card.service';

@NgModule({
  declarations: [
    BusComponent,
    CardComponent,
    PyramideComponent,
    GameControlsComponent,
  ],
  imports: [
    CommonModule,
    BusRoutingModule,
    MatButtonModule,
  ],
  providers: [
    CardService,
  ]
})
export class BusModule { }
