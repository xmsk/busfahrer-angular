import { Component, Input, OnInit } from '@angular/core';
import { Card } from '../models/card';

@Component({
  selector: 'app-pyramide',
  templateUrl: './pyramide.component.html',
  styleUrls: ['./pyramide.component.scss']
})
export class PyramideComponent implements OnInit {

  @Input() cards?: Card[];

  constructor() { }

  ngOnInit(): void {
  }

}
