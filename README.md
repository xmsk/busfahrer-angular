# BusfahrerAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.1.

## Running

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Routing

## Creating a new top-level module with routing

* generate a new module using `ng generate module <module_name> --module app --routing`
* go to the newly created directory `<module_name>` and generate a main component using `ng generate component <component_name> -m <module_name>`
* go to `app-routing.module.ts` and create an entry in the `routes` list to lazily load the newly created module
* go to `<module_name>.module.ts` and create a default route pointing to the new component `<component_name>`
* go to `app.component.ts` to create a nav entry for the newly created page

## Creating a new top-level module without routing

* generate a new module using `ng generate module <module_name> --module app`
* go to the newly created directory `<module_name>` and generate a main component using `ng generate component <component_name> -m <module_name>`
* go to `app-routing.module.ts` and create an entry in the `routes` list and point the component to the newly created main component `<component_name>`
* go to `app.component.ts` to create a nav entry for the newly created page
